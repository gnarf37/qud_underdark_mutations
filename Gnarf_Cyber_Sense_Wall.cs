using System;

namespace XRL.World.Parts
{

    [Serializable]
    public class Gnarf_Cyber_Sense_Wall : IPart
    {
        public Guid ActivatedAbilityID = Guid.Empty;

        [NonSerialized] public GameObject _target = null;
        public GameObject target
        {
            get
            {
                if (_target == null || _target.IsInvalid())
                {
                    _target = ParentObject.GetPart<CyberneticsBaseItem>().ImplantedOn;
                }
                return _target;
            }
        }

        public override bool SameAs(IPart p)
        {
            return false;
        }

        public static int Radius = 9;

        public override bool AllowStaticRegistration()
        {
            return true;
        }
        public override bool WantEvent(int ID, int cascade)
        {
            return base.WantEvent(ID, cascade) ||
                ID == BeforeRenderEvent.ID;
        }

        public override bool HandleEvent(BeforeRenderEvent E)
        {
            GameObject subject = ParentObject.Implantee;
            if (
                subject != null
                && subject.IsPlayer()
            )
            {
                Cell C = subject.CurrentCell;
                if (C != null && !C.OnWorldMap())
                {
                    var walls = C.ParentZone.FastSquareSearch(C.X, C.Y, Radius, go => 
                    {
                        return go.HasTag("Wall");
                    });
                    if (walls != null) foreach (var wall in walls)
                    {
                        C.ParentZone?.AddLight(wall.CurrentCell.X, wall.CurrentCell.Y, 0, LightLevel.Omniscient);
                    }
                }
            }
            return base.HandleEvent(E);
        }

        public override void Register(GameObject Object, IEventRegistrar Registrar)
        {
            Registrar.Register("Implanted");
            Registrar.Register("Unimplanted");
            base.Register(Object, Registrar);
        }

        public override bool FireEvent(Event E)
        {
            if (E.ID == "Implanted")
            {
                GameObject target = E.GetGameObjectParameter("Object");
                target.RegisterPartEvent(this, "EndTurn");
            }
            else
            if (E.ID == "Unimplanted")
            {
                GameObject target = E.GetGameObjectParameter("Object");
                target.UnregisterPartEvent(this, "EndTurn");
                _target = null;
            }

            return base.FireEvent(E);
        }

    }

}
