using System;

namespace XRL.World.Parts
{
    [Serializable]
    public class Gnarf_CaverCorpseLoot : IPart
    {
        public Gnarf_CaverCorpseLoot()
        {
        }

        public override bool SameAs(IPart p)
        {
            return false;
        }

        public override void Register(GameObject Object, IEventRegistrar Registrar)
        {
            Registrar.Register("EnteredCell");
            base.Register(Object, Registrar);
        }

        public bool bCreated = false;
        public override bool FireEvent(Event E)
        {
            if (E.ID == "EnteredCell")
            {
                if (bCreated) return true;
                bCreated = true;

                Physics pPhysics = ParentObject.GetPart("Physics") as Physics;
                pPhysics.CurrentCell.AddObject(GameObjectFactory.Factory.CreateObject("Gnarf_CPS"));
                pPhysics.CurrentCell.AddObject(GameObjectFactory.Factory.CreateObject("Gnarf_FFG"));
                pPhysics.CurrentCell.AddObject(GameObjectFactory.Factory.CreateObject("Gnarf_FFG"));
                ParentObject.UnregisterPartEvent(this, "EnteredCell");

                return true;
            }

            return base.FireEvent(E);
        }
    }
}
