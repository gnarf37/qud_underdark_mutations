using System;
using XRL.World.Effects;

namespace XRL.World.Parts.Mutation
{
    [Serializable]
    class Gnarf_Sunlight_Sensitivity : BaseMutation
    {
        bool WarningState = false;

        public Gnarf_Sunlight_Sensitivity()
        {
            DisplayName = "Sunlight Sensitivity (&rD&y)";
        }
 
        public override void Register(GameObject Object, IEventRegistrar Registrar) {
            Registrar.Register("EndTurn");
            Registrar.Register("GetLostChance");
            base.Register(Object, Registrar);
        }

        public override bool CanLevel()
        {
            return false;
        }
         
        public override string GetDescription()
        {
            string Ret = 
                "Your eyes are adapted to darkness.\n\n" +
                "While outdoors during daylight, or near very bright light sources, " +
                "there is a chance every round of being Dazed by the brightness.\n";
            return Ret;

        }
 
        public override string GetLevelText(int Level)
        {
            return "";
        }
 
        protected void Warning(bool v, string why = "") {
            if (WarningState != v) {
                WarningState = v;
                if (WarningState) {
                    AddPlayerMessage("&WThe light is very bright here, you can hardly focus.  " + why);
                } else {
                    AddPlayerMessage("&wYour eyes welcome the &Kdarkness&W");
                }
            }
        }

        protected void OuchLights() {
            AddPlayerMessage("&YThe light is too bright, dazing you temporarily.");

            ParentObject.ApplyEffect(new Dazed(Rules.Stat.Random(3, 8)));
        }
 
        public override bool FireEvent(Event E)
        {
            if (E.ID == "GetLostChance") {
                if (ParentObject.Physics == null) return true;
                Cell C = ParentObject.Physics.CurrentCell;
                if (C==null) return true;

                if (IsDay()) {
                    AddPlayerMessage("The &Wsun&y bleaches your vision making it hard to find your way.");
                    E.SetParameter("Amount", E.GetParameter<int>("Amount") + 25);
                }
                return true;
            }

            if (E.ID == "EndTurn") {

                if (ParentObject.Physics == null) return true;
                Cell C = ParentObject.Physics.CurrentCell;
                if (C==null) return true;

                // on the surface: Daylight burns our eyes!
                if (ParentObject.IsUnderSky() && IsDay()) {
                    Warning(true, "The Sun is shining directly on you.");
                    if (!C.ParentZone.IsWorldMap() && Rules.Stat.Random(1, 100) <= 10) {
                        // Avoid "stun" if we are on world map hopefully
                        OuchLights();
                    }
                    return true;
                }

                bool triggered = false;

                C.GetAdjacentCells(15).ForEach(otherCell => { 
                    if (!triggered) {
                        otherCell.GetObjectsWithPart("LightSource").ForEach( o => {
                            if (ParentObject.HasLOSTo(o)) {
                                var light = o.GetPart<LightSource>();
                                int brightness = light.Radius - C.DistanceTo(otherCell.X, otherCell.Y);
                                // AddPlayerMessage("Debug: Light: " + light.Radius + " Distance: " + C.DistanceTo(otherCell));
                                if (light.Lit && brightness >= 5) {
                                    triggered = true;
                                }
                            }
                        });
                    }
                });
                if (triggered) {
                    Warning(true, "You are too close to a bright light");
                    // Less likely than sunlight.
                    if (Rules.Stat.Random(1, 100) <= 5) {
                        OuchLights();
                    }
                    return true;
                }

                ParentObject.ForeachEquippedObject(o => {
                    var light = o.GetPart<LightSource>();
                    if (light != null) {
                        // AddPlayerMessage("Debug: Equipped Light: " + light.Radius);
                        if (light.Lit && light.Radius >= 5) {
                            triggered = true;
                        }
                    }
                });
                if (triggered) {
                    Warning(true, "You are carrying a bright light!");
                    // this is the dumbest, 20% chance to daze!
                    if (Rules.Stat.Random(1, 100) <= 20) {
                        OuchLights();
                    }
                    return true;
                }

                Warning(false);
                return true;
            }
            return base.FireEvent(E);
        }
 
        public override bool ChangeLevel(int NewLevel)
        {
            return true;
        }
 
        public override bool Mutate(GameObject GO, int Level)
        {
            return true;
        }
 
        public override bool Unmutate(GameObject GO)
        {
            return true;
        }
    }
}