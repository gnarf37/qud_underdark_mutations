using ConsoleLib.Console;
using System.Collections.Generic;
using System;
using XRL.UI;

namespace XRL.World.Parts.Mutation
{

    [Serializable]
    public class Gnarf_FeyFire : BaseMutation
    {

        public Guid FFAbilityId = Guid.Empty;
        public ActivatedAbilityEntry FFAbilityEntry = null;

        public Gnarf_FeyFire()
        {
            DisplayName = "Fey Fire";
            Type = "Mental";
        }

        public override void Register(GameObject Object, IEventRegistrar Registrar)
        {
            Registrar.Register("CommandGnarf_FeyFire");
            Registrar.Register("AIGetOffensiveMutationList");
            base.Register(Object, Registrar);
        }

        public override string GetDescription()
        {
            return "You focus the natural light energy to summon a ghostly flame which marks enemies and lights areas.";
        }

        public int GetDVPenaltyForLevel(int Level)
        {
            return (3 + Level) / -2;
        }
        public int GetGasDensityForLevel(int Level)
        {
            return 25 + Level * 25;
        }

        public override string GetLevelText(int Level)
        {
            string Ret =
                "The fire is like a gas and will spread from it's inital impact zone, and any creature walking " +
                "through the affected area must make an Agility Save or be marked with fire and easier to see as " +
                "well as becoming a beacon of light.\n\n" +
                "Range: 8\n" +
                "DV Penalty for Marked Creatures: " + GetDVPenaltyForLevel(Level);
            return Ret;
        }
        
        public void FeyUp(List<Cell> Cells)
        {
            TextConsole _TextConsole = UI.Look._TextConsole;
            ScreenBuffer Buffer = TextConsole.ScrapBuffer;
            Core.XRLCore.Core.RenderMapToBuffer(Buffer);

            foreach (Cell C in Cells)
            {
                GameObject Fire = GameObjectFactory.Factory.CreateObject("Gnarf_FeyFireGas");

                Gas pGas = Fire.GetPart("Gas") as Gas;
                pGas.Level = Level;
                pGas.Density = GetGasDensityForLevel(Level);

                Gnarf_Gas_FeyFire pFFGas = Fire.GetPart("Gnarf_Gas_FeyFire") as Gnarf_Gas_FeyFire;
                pFFGas.Owner = ParentObject;
                C.AddObject(Fire);

                if (C.IsVisible())
                {
                    Buffer.Goto(C.X, C.Y);
                    if (Rules.Stat.Random(1, 3) == 1)
                    {
                        Buffer.Write("&M*");
                    }
                    else
                    if (Rules.Stat.Random(1, 3) == 1)
                    {
                        Buffer.Write("&m*");
                    }
                    else
                    {
                        Buffer.Write("&K^m*");
                    }

                    _TextConsole.DrawBuffer(Buffer);
                    System.Threading.Thread.Sleep(25);
                }
            }
        }

        public static bool Cast( Gnarf_FeyFire mutation = null, string level = "5-6")
        {
            if (mutation == null)
            {
                mutation = new Gnarf_FeyFire();
                mutation.Level = Rules.Stat.Roll(level);
                mutation.ParentObject = XRL.Core.XRLCore.Core.Game.Player.Body;
            }

            List<Cell> TargetCell = mutation.PickBurst(1, 8, false, AllowVis.Any);
            if (TargetCell == null) return true;

            foreach (Cell C in TargetCell)
                if (C.DistanceTo(mutation.ParentObject) > 9)
                {
                    if (mutation.ParentObject.IsPlayer())
                    {
                        Popup.Show("That is out of range! (8 squares)");
                    }

                    return true;
                }

            if (TargetCell != null)
            {
                if(mutation.FFAbilityEntry != null ) mutation.FFAbilityEntry.Cooldown = 1000;

                mutation.FeyUp(TargetCell);
                mutation.UseEnergy(1000, "Mental Mutation");
            }

            return true;
        }

        public override bool FireEvent(Event E)
        {
            if (E.ID == "AIGetOffensiveMutationList")
            {
                int Distance = E.GetIntParameter("Distance");

                if (FFAbilityEntry != null && FFAbilityEntry.Cooldown <= 0 && Distance <= 8)
                {
                    List<AI.GoalHandlers.AICommandList> CommandList = E.GetParameter("List") as List<AI.GoalHandlers.AICommandList>;
                    CommandList.Add(new AI.GoalHandlers.AICommandList("CommandGnarf_FeyFire", 1));
                }
            }
            else
            if (E.ID == "CommandGnarf_FeyFire")
            {
                if (!Cast(this)) return false;
            }
            return base.FireEvent(E);
        }

        public override bool ChangeLevel(int NewLevel)
        {
            return base.ChangeLevel(NewLevel);
        }

        public override bool Mutate(GameObject GO, int Level)
        {
            Unmutate(GO);

            ActivatedAbilities pAA = GO.GetPart("ActivatedAbilities") as ActivatedAbilities;

            FFAbilityId = pAA.AddAbility("Fey Fire", "CommandGnarf_FeyFire", "Mental Mutation", Icon: "*");
            FFAbilityEntry = pAA.AbilityByGuid[FFAbilityId];
            return base.Mutate(GO, Level);
        }

        public override bool Unmutate(GameObject GO)
        {
            if (FFAbilityId != Guid.Empty)
            {
                ActivatedAbilities pAA = GO.GetPart("ActivatedAbilities") as ActivatedAbilities;
                pAA.RemoveAbility(FFAbilityId);
                FFAbilityId = Guid.Empty;
            }
            return base.Unmutate(GO);
        }

    }

}
