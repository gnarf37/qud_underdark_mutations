using ConsoleLib.Console;
using Genkit;
using System.Collections.Generic;
using System;
using XRL.World.Parts;
using XRL.World;
using Qud.UI;

namespace XRL.UI
{
    [XRL.Wish.HasWishCommand]
    [UIView("Gnarf_UI_MapView", NavCategory: "Menu", WantsTileOver: true, ForceFullscreen: true)]
    public class Gnarf_UI_MapView : IWantsTextConsoleInit
    {
        public static TextConsole _TextConsole;
        public static ScreenBuffer _ScreenBuffer;
        public static ScreenBuffer OldBuffer = ScreenBuffer.create(80,25);
        public static ScreenBuffer Buffer = ScreenBuffer.create(80,25);
        public static bool bLocked = true; // TODO: move into serialized object

        public void Init(TextConsole textConsole, ScreenBuffer screenBuffer)
        {
            _TextConsole = textConsole;
            _ScreenBuffer = screenBuffer;
        }

        public static List<GameObject> wantsToPaint = new List<GameObject>();

        [XRL.Wish.WishCommand(Command = "map")]
        public static void WishCommand()
        {
            ShowPicker();
        }

        public static Cell ShowPicker()
        {
            GameManager.Instance.PushGameView("Gnarf_UI_MapView");
            UIManager.pushSaveStack();
            UIManager.showWindow(null, true);
            OldBuffer.Copy(TextConsole.CurrentBuffer);

            ZoneManager ZM = Core.XRLCore.Core.Game.ZoneManager;

            string WorldName = ZM.ActiveZone.GetZoneWorld();
            Zone WorldZone = ZM.GetZone(WorldName);

            Cell C = XRL.The.PlayerCell;
            if (C == null) return null;

            int StartX = C.ParentZone.IsWorldMap() ? C.X : C.ParentZone.wX;
            int StartY = C.ParentZone.IsWorldMap() ? C.Y : C.ParentZone.wY;

            bool bDone = false;
            Cell Return = null;
            Point2D startingLocation = C.Pos2D;

            int xp = StartX;
            int yp = StartY;
            bool bFirst = true;

            while (!bDone)
            {
                wantsToPaint.Clear();
                for (int x=0; x<80; x++) {
                    for (int y=0; y<25;y++) {
                        Buffer.Goto(x, y);
                        ConsoleChar Char = Buffer.Buffer[x, y];
                        Cell WZC = WorldZone.Map[x][y];
                        Char.Tile = null;
                        var ev = WZC.Render(
                            Char: Char,
                            Visible: true,
                            Lit: LightLevel.Light,
                            Explored: true,
                            Alt: false,
                            DisableFullscreenColorEffects: true,
                            WantsToPaint: wantsToPaint
                        );

                        Char.SetColors(ev);
                        Char.HFlip = ev.HFlip;
                        Char.VFlip = ev.VFlip;
                        Char._Char = ev.RenderString[0];
                        if (Char._Tile != null)
                        {
                            Char.BackupChar = Char._Char;
                            Char._Char = '\0';
                        }
                        if (Char.Tile != null)
                        {
                            Char.Char = '\0';
                        }

                    }
                }

                foreach( var go in wantsToPaint )
                {
                    go.Paint( Buffer );
                }
                Buffer.Goto(StartX, StartY);
                Buffer.Write("&R@");

                Buffer.Goto(xp, yp);
                Buffer.Write("&WX");

                if (xp < 40)
                {
                    Buffer.Goto(54, 0);
                }
                else
                {
                    Buffer.Goto(1, 0);
                }

                Buffer.Write("&Wspace&y-select target");


                _TextConsole.DrawBuffer(Buffer);

                if (ConsoleLib.Console.Keyboard.kbhit())
                {
                    Keys c = ConsoleLib.Console.Keyboard.getvk(true);

                    if (c == Keys.MouseEvent)
                    {
                        if ( Keyboard.CurrentMouseEvent.Event == "PointerOver" && !bFirst )
                        {
                            xp = Keyboard.CurrentMouseEvent.x;
                            yp = Keyboard.CurrentMouseEvent.y;
                        }

                        if (Keyboard.CurrentMouseEvent.Event == "RightClick")
                        {
                            bDone = true;
                        }
                        bFirst = false;
                    }

                    if (c == Keys.NumPad5 || c == Keys.Escape)
                    {
                        bDone = true;
                    }

                    if (c == Keys.NumPad1) { xp--; yp++; };
                    if (c == Keys.NumPad2) { yp++; };
                    if (c == Keys.NumPad3) { xp++; yp++; };
                    if (c == Keys.NumPad4) { xp--; };
                    if (c == Keys.NumPad6) { xp++; };
                    if (c == Keys.NumPad7) { xp--; yp--; };
                    if (c == Keys.NumPad8) { yp--; };
                    if (c == Keys.NumPad9) { xp++; yp--; };

                    if (c == Keys.F || c == Keys.Space || c == Keys.Enter || (c == Keys.MouseEvent && Keyboard.CurrentMouseEvent.Event == "LeftClick")) 
                    {
                            bDone = true;
                            Return = WorldZone.GetCell(xp, yp);
                    };
                    if (xp < 0) xp = 0;
                    if (xp >= WorldZone.Width) xp = WorldZone.Width - 1;
                    if (yp < 0) yp = 0;
                    if (yp >= WorldZone.Height) yp = WorldZone.Height - 1;
                }

            }

            _TextConsole.DrawBuffer( OldBuffer );
            GameManager.Instance.PopGameView();
            UIManager.popSaveStack();
            return Return;
        }
    }
}
