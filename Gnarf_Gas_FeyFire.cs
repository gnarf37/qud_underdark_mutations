using System;
using System.Linq;
using XRL.World.Effects;

namespace XRL.World.Parts
{
    [Serializable]
    public class Gnarf_Gas_FeyFire : IPart
    {

        public string GasType = "Gnarf_FeyFire";

        [NonSerialized] private static Type EffectFeyFire = ModManager.ResolveType(typeof(Gnarf_FeyFire_Effect).FullName);
        [NonSerialized] private static Type EffectLuminous = ModManager.ResolveType(typeof(Luminous).FullName);

        public GameObject Owner;

        public override bool SameAs(IPart p)
        {
            Gnarf_Gas_FeyFire o = p as Gnarf_Gas_FeyFire;
            if (o.GasType != GasType) return false;
            return base.SameAs(p);
        }

        public override void Register(GameObject Object, IEventRegistrar Registrar)
        {
            Registrar.Register("EndTurn");
            Registrar.Register("ObjectEnteredCell");
            Registrar.Register("GasSpawned");
            base.Register(Object, Registrar);
        }

        public void ApplyFeyFire(GameObject GO)
        {
            Gas pGas = ParentObject.GetPart<Gas>();
            int Density = pGas.Density;
            if (!GO.HasPart("Combat") || GO.HasPart("Gnarf_Gas_FeyFire")) {
                return;
            }
            if (Density > 0 && !GO.MakeSave("Agility", 5 + pGas.Level + Density / 10, Vs: "Fey Fire"))
            {
                int Duration = Rules.Stat.Roll((pGas.Level / 2 + 3) +"d4") + pGas.Level;
                Gnarf_FeyFire_Effect FE = GO.GetEffect<Gnarf_FeyFire_Effect>();
                if (FE != null) {
                    FE.Duration = Math.Max(Duration, FE.Duration);
                } else {
                    FE = new Gnarf_FeyFire_Effect(Duration, (pGas.Level + 3) / -2);
                    GO.ApplyEffect(FE);
                }
                Luminous LE = GO.GetEffect<Luminous>();
                if (LE != null) {
                    LE.Duration = Math.Max(Duration, LE.Duration);
                } else {
                    LE = new Luminous(Duration);
                    GO.ApplyEffect(LE);

                }
            }
        }

        private void ApplyFeyFireToOthers(GameObject GO)
        {
            if (GO != ParentObject && GO.PhaseMatches(ParentObject) && GO != Owner) ApplyFeyFire(GO);
        }

        public override bool FireEvent(Event E)
        {
            if (E.ID == "EndTurn")
            {
                Cell C = ParentObject.CurrentCell;
                if (C != null)
                {
                    C.ForeachObjectWithPart("Physics", (Action<GameObject>) ApplyFeyFireToOthers);
                }
            }
            else if (E.ID == "ObjectEnteredCell")
            {
                ApplyFeyFireToOthers(E.GetGameObjectParameter("Object"));
            }
            else if (E.ID == "GasSpawned")
            {
                Owner = E.GetParameter<GameObject>("Parent").GetPart<Gnarf_Gas_FeyFire>().Owner;
            }
            return base.FireEvent(E);
        }

    }

}
