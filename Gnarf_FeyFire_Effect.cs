using System;

namespace XRL.World.Effects
{

    [Serializable]
    public class Gnarf_FeyFire_Effect : Effect
    {

        int DVShift = -2;

        public Gnarf_FeyFire_Effect()
        {
            DisplayName = "&Mmarked with light";
        }

        public Gnarf_FeyFire_Effect(int _Duration, int _DVShift) : this()
        {
            Duration = _Duration;
            DVShift = _DVShift;
        }

        public override string GetDetails()
        {
            return "Tiny motes of light surround you making you easier to see.";
        }

        private void ApplyStats()
        {
            StatShifter.SetStatShift("DV", DVShift);
        }

        private void UnapplyStats()
        {
            StatShifter.RemoveStatShifts();
        }

        public override void Register(GameObject Object, IEventRegistrar Registrar)
        {
            Registrar.Register("EndTurn");
            base.Register(Object, Registrar);
        }


        override public bool Apply(GameObject Object) {
            if (Object.HasEffect("Gnarf_FeyFire_Effect")) {
                return false;
            }
            ApplyStats();
            return true;
        }

        public override void Remove(GameObject Object)
        {
            UnapplyStats();
        }
        public override bool Render(RenderEvent E)
        {
            if (Duration > 0) {
                int nFrame = Core.XRLCore.CurrentFrame % 60;
                if (nFrame > 5  && nFrame < 10) {
                    E.ColorString = "&m^K";
                    return false;
                }
            }
            return true;
        }

        public override bool FireEvent(Event E)
        {
            if (E.ID == "EndTurn") 
            {
                Duration--;
                if (Duration == 0) {
                    UnapplyStats();
                }
            }
            return base.FireEvent(E);
        }

    }

}
