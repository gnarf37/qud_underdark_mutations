using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using XRL.World.Effects;

namespace XRL.World.Parts.Mutation
{
    [Serializable]
    class Gnarf_Sense_Walls : BaseMutation
    {
        public Gnarf_Sense_Walls()
        {
            DisplayName = "Dweller's Sight";
        }
 
        public override bool WantEvent(int ID, int cascade)
        {
            return base.WantEvent(ID, cascade) ||
                ID == BeforeRenderEvent.ID;
        }

        public override bool HandleEvent(BeforeRenderEvent E)
        {
            var Radius = GetRadiusForLevel(Level);
            GameObject subject = ParentObject;
            if (
                subject != null
                && subject.IsPlayer()
            )
            {
                Cell C = subject.CurrentCell;
                if (C != null && !C.OnWorldMap())
                {
                    var walls = C.ParentZone.FastSquareSearch(C.X, C.Y, Radius, go => 
                    {
                        return go.HasTag("Wall");
                    });
                    if (walls != null) foreach (var wall in walls)
                    {
                        C.ParentZone?.AddLight(wall.CurrentCell.X, wall.CurrentCell.Y, 0, LightLevel.Omniscient);
                    }
                }
            }
            return base.HandleEvent(E);
        }
         
        public override string GetDescription()
        {
            string Ret = 
                "Years of living in caves below ground in the depths of Qud have given you a sixth sense " + 
                "of the location of structural walls and hidden tunnels.";
            return Ret;

        }
 
        public override string GetLevelText(int Level)
        {
            return "You detect the presence of walls within " + GetRadiusForLevel(Level) + " tiles.";
        }

        public int GetRadiusForLevel(int Level) {
            return 3 + (Level * 3 / 2);
        }
 
        public override bool ChangeLevel(int NewLevel)
        {
            return base.ChangeLevel(NewLevel);
        }
 
        public override bool Mutate(GameObject GO, int Level)
        {
            return base.Mutate(GO, Level);
        }
 
        public override bool Unmutate(GameObject GO)
        {
            return base.Unmutate(GO);
        }
    }
}