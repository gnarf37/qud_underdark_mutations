using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XRL.UI;
using Qud.API;
using XRL.World;
using XRL.World.Effects;

namespace XRL.World.Parts
{
    [Serializable]
    public class Gnarf_CPS : IPart
    {

        public static int Resolve(int wx, int x) { return wx * 3 + x; }

        public static string FormatXYZ(int x, int y, int z) {
            string Result = "";

            Result += Math.Abs(x);
            Result += x > 0 ? "E " : x < 0 ? "W " : "- ";

            Result += Math.Abs(y);
            Result += y > 0 ? "S " : y < 0 ? "N " : "| ";

            Result += Math.Abs(z);
            Result += z > 0 ? "D" : z < 0 ? "U" : "";

            return Result;
        }

        public static string FormatXYZ(int wx, int wy, int x, int y, int z) {
            string Result = "";

            Result += Math.Abs(wx);
            if (x!=0) {
                Result += "~" + Math.Abs(x);
            }
            Result += Resolve(wx,x) > 0 ? "E " : Resolve(wx, x) < 0 ? "W " : "- ";

            Result += Math.Abs(wy);
            if (y!=0) {
                Result += "~" + Math.Abs(y);
            }
            Result += Resolve(wy,y) > 0 ? "S " : Resolve(wy, y) < 0 ? "N " : "| ";

            Result += Math.Abs(z);
            Result += z > 0 ? "D" : z < 0 ? "U" : "";

            return Result;
        }

        public int TargetX = 1;
        public int TargetY = 1;
        public int TargetWorldX = 11;
        public int TargetWorldY = 22;
        public int TargetWorldZ = 10;

        public string DistanceToTarget(int wx, int wy, int x, int y, int z)
        {
            int dx = Resolve(TargetWorldX, TargetX) - Resolve(wx, x);
            int dy = Resolve(TargetWorldY, TargetY) - Resolve(wy, y);
            int dz = TargetWorldZ - z;
            return FormatXYZ(dx/3, dy/3, dx%3, dy%3, dz);
        }
        public string LastState = FormatXYZ(0, 3, -4);

        public override void Register(GameObject Object, IEventRegistrar Registrar)
        {
            Registrar.Register("GetInventoryActions");
            Registrar.Register("GetDisplayName");
            Registrar.Register("Gnarf_Command_CPS");
            Registrar.Register("Gnarf_Command_CPS_Calibrate");
            Registrar.Register("Gnarf_Command_CPS_Calibrate_Journal");
            base.Register(Object, Registrar);
        } 

        public GameObject Owner {
            get {
                return ParentObject.Equipped ?? ParentObject.InInventory;
            }
        }

        public void Scan()
        {
            Cell C = ParentObject.CurrentCell;
            if (C == null && Owner != null) {
                C = Owner.CurrentCell;
            }
            if (C == null) {
                XRL.UI.Popup.Show("Something went horribly wrong with the scanner");
                return;
            }

            int wX = C.ParentZone.IsWorldMap() ? C.X : C.ParentZone.wX;
            int wY = C.ParentZone.IsWorldMap() ? C.Y : C.ParentZone.wY;
            int X = C.ParentZone.IsWorldMap() ? 1 : C.ParentZone.X;
            int Y = C.ParentZone.IsWorldMap() ? 1 : C.ParentZone.Y;
            int Z = C.ParentZone.IsWorldMap() ? 9 : C.ParentZone.Z;

            LastState = DistanceToTarget(wX, wY, X, Y, Z);
        }

        public bool IsLost() 
        {
            var lost = Owner.GetEffect<Lost>();
            if (lost != null) {
                return lost.Duration > 0;
            }
            return false;
        }

        public override bool FireEvent(Event E)
        {
            if (E.ID == "GetDisplayName")
            {
                if (ParentObject.Understood())
                {
                    E.GetParameter<StringBuilder>("Postfix").Append(" &y[&R" + LastState + "&y]");
                }
            }
            if (E.ID == "GetInventoryActions")
            {
                if (ParentObject.Understood())
                {
                    E.GetParameter<EventParameterGetInventoryActions>("Actions")
                        .AddAction("Activate", 'a', false, "&Wa&yctivate", "Gnarf_Command_CPS");
                    E.GetParameter<EventParameterGetInventoryActions>("Actions")
                        .AddAction("Calibrate", 'C', false, "&WC&yalibrate from World Map", "Gnarf_Command_CPS_Calibrate");
                    E.GetParameter<EventParameterGetInventoryActions>("Actions")
                        .AddAction("Journal", 'J', false, "Calibrate from &WJ&yournal", "Gnarf_Command_CPS_Calibrate_Journal");
                }
            }
            else if (E.ID == "Gnarf_Command_CPS") {
                if (IsLost()) {
                    XRL.UI.Popup.Show("You are so lost the directions from the CPS make no sense");
                    return true;
                }
                if (!ParentObject.TestCharge(250)) {
                    XRL.UI.Popup.Show("The CPS makes some sad sounds, perhaps the power cell is low");
                    return true;
                }
                if (ParentObject.UseCharge(250)) {
                    Scan();
                    XRL.UI.Popup.Show("The CPS spins and whirls and its read out now says:\n&R" + LastState);
                }
                return true;
            }
            else if (E.ID == "Gnarf_Command_CPS_Calibrate") {
                if (IsLost()) {
                    XRL.UI.Popup.Show("You are so lost you can't calibrate the device");
                    return true;
                }
                if (!ParentObject.TestCharge(1000)) {
                    XRL.UI.Popup.Show("The CPS makes some sad sounds, perhaps the power cell is low");
                    return true;
                }
                if (ParentObject.UseCharge(1000)) {
                    Cell C = XRL.UI.Gnarf_UI_MapView.ShowPicker();
                    if (C != null) {
                        TargetWorldX = C.X;
                        TargetX = 1;
                        TargetWorldY = C.Y;
                        TargetY = 1;
                        TargetWorldZ = 10;
                        Scan();
                        XRL.UI.Popup.Show("The CPS spins and whirls and its read out now says:\n&R" + LastState);
                    }
                    return true;
                }
            }
            else if (E.ID == "Gnarf_Command_CPS_Calibrate_Journal") {
                if (IsLost()) {
                    XRL.UI.Popup.Show("You are so lost you can't calibrate the device");
                    return true;
                }
                GameObject O = E.GetGameObjectParameter("Owner");
                if (O == null)
                {
                    O = Owner;
                }

                List<JournalMapNote> Notes = Qud.API.JournalAPI.GetMapNotes( (note) => { return note.Revealed; } );
                if (Notes.Count == 0) {
                    XRL.UI.Popup.Show("You have no locations noted in your Journal");
                    return true;
                }
                int currentPage = 0;
                while (true) {
                    char c = 'a';
                    List<string> OptionStrings = new List<string>(16);
                    List<char> keymap = new List<char>(16);                
                    int baseIndex = currentPage * 10;
                    int index;
                    for (index = baseIndex; index < Notes.Count; index ++) {
                        OptionStrings.Add(Notes[index].GetDisplayText());
                        if (c <= 'z')
                        {
                            keymap.Add(c++);
                        }
                        else
                        {
                            keymap.Add(' ');
                        }
                    }                

                    if (Owner.IsPlayer())
                    {
                        int choice = XRL.UI.Popup.PickOption(
                            "Select a Journal Entry to Calibrate the CPS:",
                            Options: OptionStrings.ToArray(),
                            Hotkeys: keymap.ToArray(),
                            Spacing: 1,
                            AllowEscape: true,
                            RespectOptionNewlines: true,
                            MaxWidth: 74
                        );
                        if (choice == -1) {
                            return true;
                        }
                        if (choice == 10) {
                            currentPage++;
                        } else if (choice + baseIndex == Notes.Count) {
                            currentPage = 0;
                        } else {
                            if (!ParentObject.TestCharge(500)) {
                                XRL.UI.Popup.Show("The CPS makes some sad sounds, perhaps the power cell is low");
                                return true;
                            }
                            if (ParentObject.UseCharge(500)) {
                                TargetWorldX = Notes[baseIndex + choice].ParasangX;
                                TargetWorldY = Notes[baseIndex + choice].ParasangY;
                                TargetWorldZ = Notes[baseIndex + choice].ZoneZ;
                                TargetX = Notes[baseIndex + choice].ZoneX;
                                TargetY = Notes[baseIndex + choice].ZoneY;
                                Scan();
                                XRL.UI.Popup.Show("The CPS spins and whirls and its read out now says:\n&R" + LastState);
                                return true;
                            }
                        }
                    } 
                }
            }

            return base.FireEvent(E);
        }



    }
}